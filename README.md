﻿# B2B API documentation version v1
https://api.grandorf.ru/b2b

---

## /products
Торговые предложения

### /products

* **get** *(secured)*: 

### /products/{id}/customer_code

* **put** *(secured)*: Установка кода номенклатуры клиента

## /contragent_contracts
Договоры контрагентов

### /contragent_contracts

* **get** *(secured)*: 

### /contragent_contracts/{id}/customer_code

* **put** *(secured)*: Установка кода склада клиента

## /orders
Заказы

### /orders

* **get** *(secured)*: 
* **post** *(secured)*: 

### /orders/сalculate

* **put** *(secured)*: Рассчитать заказ без сохранения TODO

### /orders/{id}

* **get** *(secured)*: 
* **put** *(secured)*: 

### /orders/{id}/confirm

* **put** *(secured)*: Подтвердить заказ

### /orders/{id}/print_form

* **get** *(secured)*: Получить печатную форму счета на оплату

## /invoices
Накладные

### /invoices

* **get** *(secured)*: 

### /invoices/{id}

* **get** *(secured)*: 

## /debt
Задолженность

### /debt

* **get** *(secured)*: 

## /ping
Проверка связи

### /ping

* **get** *(secured)*: 

